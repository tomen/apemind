part of apemind.trading;


class Market {
  double _buyFeeProportion;
  double _sellFeeProportion;

  Market(this._buyFeeProportion, this._sellFeeProportion);

  CompletedOrder performBuy(AssetPair assetPair, Portfolio portfolio, double price, int time){
    Asset fundingAsset = assetPair.quote;
    Asset receivingAsset = assetPair.base;
    double fundingAssetValue = portfolio.assetValue(fundingAsset);
    if(fundingAssetValue == 0.0){
      return null;
    }

    double receivingAssetValue = fundingAssetValue / price;
    receivingAssetValue *= 1-_buyFeeProportion;

    portfolio.decreaseAssetValue(fundingAsset, fundingAssetValue); //will result in 0
    portfolio.increaseAssetValue(receivingAsset, receivingAssetValue);

    CompletedOrder order = new CompletedOrder();
    order.assetPair = assetPair;
    order.type = OrderType.BUY_LIMIT;
    order.fundingVolume = fundingAssetValue;
    order.receivingVolume = receivingAssetValue;
    order.time = time;
    order.price = price;

    return order;
  }

  CompletedOrder performSell(AssetPair assetPair, Portfolio portfolio, double price, int time){
    Asset fundingAsset = assetPair.base;
    Asset receivingAsset = assetPair.quote;
    double fundingAssetValue = portfolio.assetValue(fundingAsset);
    double currentPrice = price;
    if(fundingAssetValue == 0.0){
      return null;
    }
    //double feeProportion = 0.0;
    double receivingAssetValue = fundingAssetValue * currentPrice;
    receivingAssetValue *= 1-_sellFeeProportion;

    portfolio.decreaseAssetValue(fundingAsset, fundingAssetValue); //will result in 0
    portfolio.increaseAssetValue(receivingAsset, receivingAssetValue);

    CompletedOrder order = new CompletedOrder();
    order.assetPair = assetPair;
    order.type = OrderType.SELL_LIMIT;
    order.fundingVolume = fundingAssetValue;
    order.receivingVolume = receivingAssetValue;
    order.time = time;
    order.price = price;

    return order;
  }
}