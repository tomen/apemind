part of apemind.trading;

class OHLC {



  int time;
  double open;
  double high;
  double low;
  double close;
  double vwap;
  double volume;
  int count;

  OHLC();

  factory OHLC.fromJSON(List json){
    OHLC ohlc = new OHLC();

    ohlc.time = json[0];
    ohlc.open = double.parse(json[1]);
    ohlc.high = double.parse(json[2]);
    ohlc.low = double.parse(json[3]);
    ohlc.close = double.parse(json[4]);
    ohlc.vwap = double.parse(json[5]);
    ohlc.volume = double.parse(json[6]);
    ohlc.count = json[7];

    return ohlc;
  }

  factory OHLC.fromSerializedJSON(List json){
    OHLC ohlc = new OHLC();

    ohlc.time = json[0];
    ohlc.open = json[1];
    ohlc.high = json[2];
    ohlc.low = json[3];
    ohlc.close = json[4];
    ohlc.vwap = json[5];
    ohlc.volume = json[6];
    ohlc.count = json[7];

    return ohlc;
  }

  bool operator ==(OHLC other) {
    return this.time == other.time;
  }

  toJson(){
    return [time, open, high, low, close, vwap, volume, count];
  }
}