part of apemind.trading;


class AssetPair {
  String code;
  Asset base;
  Asset quote;
  AssetPair._internal(this.code, this.base, this.quote);
  String toString(){
    return this.code;
  }

  static Map<String, AssetPair> _assetPairs = {};

  factory AssetPair.fromKrakenRawData(String code, Map rawAssetPair){
    if(_assetPairs[code] != null) return _assetPairs[code];

    Asset base = new Asset(rawAssetPair["base"]);
    Asset quote = new Asset(rawAssetPair["quote"]);

    AssetPair assetPair = new AssetPair._internal(code, base, quote);
    _assetPairs[code] = assetPair;

    return assetPair;
  }

  factory AssetPair.fromSerializedJSON(List raw){
    Asset base = new Asset(raw[1]);
    Asset quote = new Asset((raw[2]));
    return new AssetPair._internal(raw[0], base, quote);
  }

  toJson(){
    return [code, base.code, quote.code];
  }

  bool operator == (other) => other is AssetPair && other.code == code;
  int get hashCode => code.hashCode;

}