part of apemind.trading;

logReturn(double current, double previous){
  if(current == previous) return 0.0;

  if(current == 0.0){
    return -10.0;
  }

  if(previous == 0.0){
    return 10.0;
  }
  return math.log(current/previous);
}

Map calculateLogReturns(Map<AssetPair, List<OHLC>> assetPairOHLCs){

  // a list of inputs for each time frame
  List inputFrames = [];
  // a list of outputs for each time frame
  List expectedOutputFrames = [];


  AssetPairTimeSeriesHelper helper = new AssetPairTimeSeriesHelper(assetPairOHLCs);
  List<AssetPair> assetPairOrder = assetPairOHLCs.keys.toList();
  int numMoments = helper.timeline.length;
  Map<AssetPair, OHLC> previousAssetPairOHLCs = null;

  for(int i = 0; i < numMoments; i++){
    int time = helper.timeline[i];
    Map<AssetPair, OHLC> currentAssetPairOHLCs = helper.assetPairOHLCsForMoment(time);
    List inputFrame = [];
    List expectedOutputFrame = [];

    // if this is the first time frame
    if(previousAssetPairOHLCs == null){
      previousAssetPairOHLCs = currentAssetPairOHLCs;
      continue;
    }

    for(AssetPair ap in assetPairOrder){
      if(currentAssetPairOHLCs.containsKey(ap)){
        OHLC currentOHLC = currentAssetPairOHLCs[ap];
        OHLC previousOHLC = previousAssetPairOHLCs[ap];
        if(previousOHLC == null){
          inputFrame.addAll([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]);
          expectedOutputFrame.add(0.0);
          continue; //this AP might not have existed in the previous frame
        }



        double logReturnOpen = logReturn(currentOHLC.open, previousOHLC.open);
        double logReturnHigh = logReturn(currentOHLC.high, previousOHLC.high);
        double logReturnLow = logReturn(currentOHLC.low, previousOHLC.low);
        double logReturnClose = logReturn(currentOHLC.close, previousOHLC.close);
        double logReturnVWAP = logReturn(currentOHLC.vwap, previousOHLC.vwap);
        double logReturnVolume = logReturn(currentOHLC.volume, previousOHLC.volume);
        double logReturnCount = logReturn(currentOHLC.count.toDouble(), previousOHLC.count.toDouble());

        inputFrame.addAll([
          logReturnOpen,
          logReturnHigh,
          logReturnLow,
          logReturnClose,
          logReturnVWAP,
          logReturnVolume,
          logReturnCount
        ]);

        expectedOutputFrame.addAll([logReturnClose]);
      }
      else {
        inputFrame.addAll([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]);
        expectedOutputFrame.add(0.0);
      }

    }

    assert(inputFrame.length == assetPairOHLCs.keys.length * 7);
    assert(expectedOutputFrame.length == assetPairOHLCs.keys.length, "expectedOutputFrame is length ${expectedOutputFrame.length}");

    assert(!inputFrame.contains(double.NEGATIVE_INFINITY));

    inputFrames.add(inputFrame);
    expectedOutputFrames.add(expectedOutputFrame);
    previousAssetPairOHLCs = currentAssetPairOHLCs;
  }

  return {"inputs": inputFrames, "outputs": expectedOutputFrames};

}


class AssetPairTimeSeriesHelper {

  Map<int, Map<AssetPair, OHLC>> _timedAssetPairOHLCs = {};
  List<int> _timeline = [];

  AssetPairTimeSeriesHelper(Map<AssetPair, List<OHLC>> assetPairOLHCs){
    Set moments = new Set();

    for(AssetPair ap in assetPairOLHCs.keys){
      List<OHLC> OHLCs = assetPairOLHCs[ap];
      for(OHLC ohlc in OHLCs){
        moments.add(ohlc.time);
        if(!_timedAssetPairOHLCs.containsKey(ohlc.time)){
          _timedAssetPairOHLCs[ohlc.time] = {};
        }
        _timedAssetPairOHLCs[ohlc.time][ap] = ohlc;
      }
    }

    _timeline = moments.toList();
    _timeline.sort();
  }

  List<int> get timeline => _timeline;

  Map<AssetPair, OHLC> assetPairOHLCsForMoment(int time){
    return _timedAssetPairOHLCs[time];
  }
}