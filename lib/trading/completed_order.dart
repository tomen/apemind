part of apemind.trading;

enum OrderType {
  BUY_LIMIT,
  SELL_LIMIT
}

class CompletedOrder {
  int time;
  AssetPair assetPair;
  OrderType type;
  double fundingVolume;
  double receivingVolume;
  double price;
}