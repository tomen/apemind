part of apemind.model;



class SingleAssetPairTradingStrategyTournamentResult {
  Map<AssetPair, List<CompletedOrder>> completedOrders = {};
  Map<AssetPair, Portfolio> finalPortfolios = {};

  getAssetPairPerformance(AssetPair assetPair, double lastAssetPairPrice, double initialUSDValue) {
    Portfolio portfolio = finalPortfolios[assetPair];

    double portfolioUSDValue = 0.0;
    // get a list of assets
    // for each asset select a asset/usd (or asset/bitcoin/usd) pairing to calculate the USD price
    for(Asset asset in portfolio.assets){

      // if the asset has a value of 0, we ignore it
      double assetValue = portfolio.assetValue(asset);
      if(assetValue == 0.0){
        continue;
      }

      // we just return pure USD values
      if(asset == Asset.USD){
        portfolioUSDValue += portfolio.assetValue(asset);
        continue;
      }

      double price = lastAssetPairPrice;
      double usdValue = assetValue * price;
      portfolioUSDValue += usdValue;
    }

    return portfolioUSDValue / initialUSDValue;
  }

  /// returns the performance ratio
  double getOverallUSDPerformance(Map<AssetPair, double> lastAssetPairPrices, double initialUSDValue){

    double sum = 0.0;
    int count = 0;

    // we calculate the performance over each AssetPair
    for(AssetPair assetPair in finalPortfolios.keys){
      double performance = getAssetPairPerformance(assetPair, lastAssetPairPrices[assetPair], initialUSDValue);
      sum += performance;
      count++;
    }

    if(count == 0){
      return 1.0;
    }

    return sum / count;
  }
}


class SingleAssetPairTradingStrategyTournament {
  List<TradingStrategy> strategies;
  Portfolio portfolioTemplate;
  Map<AssetPair, List<OHLC>> assetPairOLHCs;
  Market market;

  Map<TradingStrategy, SingleAssetPairTradingStrategyTournamentResult> run(){
    Map<TradingStrategy, SingleAssetPairTradingStrategyTournamentResult> tournamentResult = {};
    // test each network...
    for(TradingStrategy strategy in strategies){
      assert(strategy != null);
      strategy.reset();
      SingleAssetPairTradingStrategyTournamentResult result = new SingleAssetPairTradingStrategyTournamentResult();
      tournamentResult[strategy] = result;

      // ...on each asset pair...
      for(AssetPair assetPair in assetPairOLHCs.keys){
        List<OHLC> OHLCs = assetPairOLHCs[assetPair];
        Portfolio portfolio = portfolioTemplate.copy();
        result.finalPortfolios[assetPair] = portfolio;
        List<CompletedOrder> completedOrders = [];
        result.completedOrders[assetPair] = completedOrders;
        //... against history
        for(OHLC ohlc in OHLCs){
          strategy.receiveInformation({assetPair: ohlc});

          CompletedOrder completedOrder = null;
          switch(strategy.currentTradeSignals[assetPair]){
            case TradeSignal.Buy:
              completedOrder = market.performBuy(assetPair, portfolio, ohlc.close, ohlc.time);
              break;
            case TradeSignal.Sell:
              completedOrder = market.performSell(assetPair, portfolio, ohlc.close, ohlc.time);
              break;
            case TradeSignal.Nothing:
              break;
          }

          if(completedOrder != null){
            completedOrders.add(completedOrder);
          }
        }
      }
    }

    return tournamentResult;
  }
}