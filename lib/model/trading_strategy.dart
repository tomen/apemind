part of apemind.model;


enum TradeSignal {
  Buy,
  Nothing,
  Sell
}

abstract class TradingStrategy {
  Map<AssetPair, TradeSignal> get currentTradeSignals;
  receiveInformation(Map<AssetPair, OHLC> assetPairOHLCs);
  reset();
}