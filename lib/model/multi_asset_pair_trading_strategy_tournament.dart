part of apemind.model;

class MultiAssetPairTradingStrategyTournamentResult {
  List<CompletedOrder> completedOrders = [];
  Portfolio finalPortfolio;

  getAssetPairPerformance(AssetPair assetPair, double lastAssetPairPrice, double initialUSDValue) {
    double portfolioUSDValue = 0.0;
    // get a list of assets
    // for each asset select a asset/usd (or asset/bitcoin/usd) pairing to calculate the USD price
    for(Asset asset in finalPortfolio.assets){

      // if the asset has a value of 0, we ignore it
      double assetValue = finalPortfolio.assetValue(asset);
      if(assetValue == 0.0){
        continue;
      }

      // we just return pure USD values
      if(asset == Asset.USD){
        portfolioUSDValue += finalPortfolio.assetValue(asset);
        continue;
      }

      double price = lastAssetPairPrice;
      double usdValue = assetValue * price;
      portfolioUSDValue += usdValue;
    }

    return portfolioUSDValue / initialUSDValue;
  }
/*
  /// returns the performance ratio
  double getOverallUSDPerformance(Map<AssetPair, double> lastAssetPairPrices, double initialUSDValue){

    double sum = 0.0;
    int count = 0;

    // we calculate the performance over each AssetP
    for(AssetPair assetPair in finalPortfolio.assets){
      double performance = getAssetPairPerformance(assetPair, lastAssetPairPrices[assetPair], initialUSDValue);
      sum += performance;
      count++;
    }

    if(count == 0){
      return 1.0;
    }

    return sum / count;
  }
  */
}

class MultiAssetPairTradingStrategyTournament {
  List<TradingStrategy> strategies;
  Portfolio portfolioTemplate;
  Map<AssetPair, List<OHLC>> assetPairOHLCs;
  Market market;

  Map<TradingStrategy, MultiAssetPairTradingStrategyTournamentResult> run(){
    Map<TradingStrategy, MultiAssetPairTradingStrategyTournamentResult> tournamentResult = {};
    AssetPairTimeSeriesHelper helper = new AssetPairTimeSeriesHelper(assetPairOHLCs);

    // test each network...
    for(TradingStrategy strategy in strategies){
      assert(strategy != null);
      strategy.reset();
      MultiAssetPairTradingStrategyTournamentResult result = new MultiAssetPairTradingStrategyTournamentResult();
      tournamentResult[strategy] = result;
      Portfolio portfolio = portfolioTemplate.copy();
      result.finalPortfolio = portfolio;

      // go through time
      //TODO: do not consider all of time!
      for(int moment in helper.timeline){
        Map<AssetPair, OHLC> currentAssetPairOHLCs = helper.assetPairOHLCsForMoment(moment);
        strategy.receiveInformation(currentAssetPairOHLCs);
        for(AssetPair ap in strategy.currentTradeSignals.keys){
          OHLC ohlc = currentAssetPairOHLCs[ap];
          TradeSignal ts = strategy.currentTradeSignals[ap];

          CompletedOrder completedOrder = null;
          switch(ts){
            case TradeSignal.Buy:
              completedOrder = market.performBuy(ap, portfolio, ohlc.close, ohlc.time);
              break;
            case TradeSignal.Sell:
              completedOrder = market.performSell(ap, portfolio, ohlc.close, ohlc.time);
              break;
            case TradeSignal.Nothing:
              break;
          }

          if(completedOrder != null){
            result.completedOrders.add(completedOrder);
          }

        }
      }
    }

    return tournamentResult;
  }
}
