library apemind.model.trading_strategies.neural_network.trainer;

import "dart:io" as io;
import "dart:math" as math;
import "dart:convert";
import "package:logging/logging.dart" as logging;

import "package:neural_network/neural_network.dart";

import "package:apemind/core.dart";
import "package:apemind/trading.dart";
import 'package:apemind/model.dart';
import "package:apemind/model/trading_strategies/neural_network_trading_strategy.dart";
import "package:apemind/model/trading_strategies/hodl_trading_strategy.dart";


const double PORTFOLIO_USD_INITIAL_VALUE = 1000.0;
const int TARGET_POPULATION_SIZE = 100;
const int NUM_INITIAL_STRATEGIES = TARGET_POPULATION_SIZE;
const int NUM_NEW_RANDOM_STRATEGIES_PER_ROUND = 100;
Map<double, int> REPRODUCTION_RATES = {0.1: 3, 0.5: 1};
const double MUTATION_RATE = 0.1;
const double MUTATION_STRENGTH = 1.0;
//const int NUM_NETWORKS_TO_MUTATE = 500;
const int NUM_TOURNAMENTS = 10;
const int NUM_FEEDFORWARD_VALUES = 10;

Portfolio portfolioTemplate = new Portfolio({Asset.USD: 1000.0});

Market market = new Market(0.0016, 0.0026);


class LaggedGeneticAlgorithmHyperparameterSet {
  List<AssetPair> _assetPairs;
  List<AssetPair> get assetPairs => new List.from(_assetPairs);
  int _momentsPerFrame;
  int get momentsPerFrame => _momentsPerFrame;
  int _numOutputsPerAssetPair;
  int get numOutputsPerAssetPair => _numOutputsPerAssetPair;
  List _networkConfiguration;
  List get networkConfiguration => _networkConfiguration;


  LaggedGeneticAlgorithmHyperparameterSet(
      this._assetPairs,
      this._momentsPerFrame,
      this._numOutputsPerAssetPair,
      List hiddenLayerConfigurationPerAssetPair){
    assert(_numOutputsPerAssetPair <= 3);

    _networkConfiguration = [numInputs];

    List hiddenLayerConfiguration = new List.generate(hiddenLayerConfigurationPerAssetPair.length, (i){
      int value = hiddenLayerConfigurationPerAssetPair[i];
      return value * _assetPairs.length;
    });

    _networkConfiguration.addAll(hiddenLayerConfiguration);
    _networkConfiguration.add(numOutputs);
  }

  int get numInputs => _assetPairs.length * 7 * momentsPerFrame;
  int get numOutputs => _assetPairs.length * _numOutputsPerAssetPair;

  Map serialize(){

    List rawAssetPairs = new List.generate(_assetPairs.length, (i){
      return _assetPairs[i].toJson();
    });

    return {
      "rawAssetPairs": rawAssetPairs,
      "_momentsPerFrame": _momentsPerFrame,
      "_numOutputsPerAssetPair": _numOutputsPerAssetPair,
      "_networkConfiguration": _networkConfiguration
    };
  }

  LaggedGeneticAlgorithmHyperparameterSet.deserialize(Map raw){
    List rawAssetPairs = raw["rawAssetPairs"];
    _assetPairs = new List.generate(rawAssetPairs.length, (i){
      return new AssetPair.fromSerializedJSON(rawAssetPairs[i]);
    });
    _momentsPerFrame = raw["_momentsPerFrame"];
    _numOutputsPerAssetPair = raw["_numOutputsPerAssetPair"];
    _networkConfiguration = raw["_networkConfiguration"];
  }
}

class LaggedGeneticAlgorithm {
  static logging.Logger _log = new logging.Logger("apemind.model.GeneticAlgorithm");

  String _branch;
  List<MonkeyMind> _networks;
  Map<AssetPair, List<OHLC>> _assetPairOHLCs;

  LaggedGeneticAlgorithm(this._assetPairOHLCs, [this._branch = "LaggedGeneticAlgorithmDefault", this._networks]);

  runOfflineTraining(){
    if(_networks == null){
      _loadNetworks();
    }

    if(_networks == null){
      _generateConversativeSeed();
    }

    //List inputNetworks = [];

    //establish training data
    Map<String, List> assetPairCombinationInputFrames = {};
    Map<String, List> assetPairCombinationExpectedOutputFrames = {};
    Map<LaggedGeneticAlgorithmHyperparameterSet, List> hyperparamInputNetworks = {};
    Map<LaggedGeneticAlgorithmHyperparameterSet, DataSet> trainingDataSets = {};

    for(MonkeyMind mm in _networks){
      LaggedGeneticAlgorithmHyperparameterSet hyperparams = mm.hyperparameters;
      List<AssetPair> assetPairCombination = hyperparams.assetPairs;
      Map<AssetPair, List<OHLC>> assetPairOHLCs = new Map.fromIterable(assetPairCombination, value: (ap) {
        return _assetPairOHLCs[ap];
      });

      List inputFrames;
      List expectedOutputFrames;

      if(!assetPairCombinationInputFrames.containsKey(assetPairCombination)){
        Map logReturns = calculateLogReturns(assetPairOHLCs);
        inputFrames = logReturns["inputs"];
        expectedOutputFrames = logReturns["outputs"];

        assetPairCombinationInputFrames[assetPairCombination.toString()] = inputFrames;
        assetPairCombinationExpectedOutputFrames[assetPairCombination.toString()] = expectedOutputFrames;
      }
      else {
        inputFrames = assetPairCombinationInputFrames[assetPairCombination.toString()];
        expectedOutputFrames = assetPairCombinationExpectedOutputFrames[assetPairCombination.toString()];
      }

      List hyperparamInputNetwork = _constructTrainingInputNetworks(hyperparams, inputFrames);
      hyperparamInputNetworks[hyperparams] = hyperparamInputNetwork;
      trainingDataSets[hyperparams] = _constructDataSet(hyperparams, hyperparamInputNetwork, expectedOutputFrames);
    }


    int i = 0;
    while(true){
      bool detailedAnalysis = i % 100 == 0;

      for(MonkeyMind mm in _networks){
        DataSet dataSet = trainingDataSets[mm.hyperparameters];
        double lastError = _trainEpoch(mm, dataSet);
        var inputNetworks = hyperparamInputNetworks[mm.hyperparameters];
        var expectedOutputFrames = assetPairCombinationExpectedOutputFrames[mm.hyperparameters.assetPairs.toString()];
        _analyzePerformance(inputNetworks, expectedOutputFrames, lastError, mm.hyperparameters, mm, detailedAnalysis);
      }

      if(detailedAnalysis){
        _saveNetworks();
        _log.info("=== $i epochs completed ===");
      }

      i++;
    }

  }

  List _constructTrainingInputNetworks(
      LaggedGeneticAlgorithmHyperparameterSet hyperparameters,
      List inputFrames)
  {
    List inputNetworks = [];

    int referenceMomentIndex = hyperparameters.momentsPerFrame;
    while(referenceMomentIndex < inputFrames.length){
      int startIndex = referenceMomentIndex - hyperparameters.momentsPerFrame;
      List input = [];

      for(int i = startIndex; i < startIndex + hyperparameters.momentsPerFrame; i++){
        input.addAll(inputFrames[i]);
      }

      inputNetworks.add(input);

      referenceMomentIndex++;
    }

    return inputNetworks;
  }

  _constructDataSet(
      LaggedGeneticAlgorithmHyperparameterSet hyperparameters,
      List inputNetwork,
      List expectedOutputFrames
      ){

    DataSet dataSet = new DataSet(
        "SupervisedDataSet",
        hyperparameters.numInputs,
        numClassValues: hyperparameters.numOutputs);

    int referenceMomentIndex = hyperparameters.momentsPerFrame;
    while(referenceMomentIndex < expectedOutputFrames.length) {
      // establish training data
      int startIndex = referenceMomentIndex - hyperparameters.momentsPerFrame;
      List input = inputNetwork[startIndex];

      List expectedOutput = expectedOutputFrames[referenceMomentIndex];
      expectedOutput = expectedOutput.take(hyperparameters.numOutputs).toList();

      List union = [];
      union.addAll(input);
      union.addAll(expectedOutput);

      if(union.length != dataSet.numClassValues + dataSet.numValues){
        int pi = 3;
      }

      dataSet.addInstance(union, true);

      referenceMomentIndex++;
    }

    return dataSet;
  }

  _trainEpoch(MonkeyMind mm, DataSet dataSet){
    BackPropagationLearningRule learningRule = new BackPropagationLearningRule(1);
    learningRule.network = mm;
    learningRule.errorFunction = new MeanSquareError();
    learningRule.learningRate = 0.03;

    //_log.info("pew... that took some time");
    learningRule.learn(dataSet);
    learningRule.currentIteration = 0;

    List<double> errorIterationsMultilayer = learningRule.errorIterations;

    return errorIterationsMultilayer.last;
  }


  _analyzePerformance(
      List inputNetworks,
      List expectedOutputFrames,
      double lastError,
      LaggedGeneticAlgorithmHyperparameterSet hyperparameters,
      MonkeyMind mm,
      bool detailed){

    if(detailed) {
      _log.info("${mm.name}");
      _log.info("error: $lastError");

      int referenceMomentIndex = hyperparameters.momentsPerFrame;
      int totalMoments = 0;
      List directionalSuccesses = new List.filled(hyperparameters.numOutputs, 0);

      while (referenceMomentIndex < expectedOutputFrames.length) {
        mm.inputNetwork =
        inputNetworks[referenceMomentIndex - hyperparameters.momentsPerFrame];
        mm.calculateOutput();
        List outputNetwork = mm.outputNetwork;
        List expectedOutput = expectedOutputFrames[referenceMomentIndex];

        assert(outputNetwork.length == expectedOutput.length);

        for(int i = 0; i < outputNetwork.length; i++){
          bool directionalSuccess;

          directionalSuccess =
              (outputNetwork[i] > 0 && expectedOutput[i] > 0) ||
                  (outputNetwork[i] < 0 && expectedOutput[i] < 0);

          if (directionalSuccess) {
            directionalSuccesses[i]++;
          }

        }

        totalMoments++;
        referenceMomentIndex++;
      }

      List<double> directionalAccuracies = new List.generate(directionalSuccesses.length, (i){
        return directionalSuccesses[i] / totalMoments;
      });

      double directionalAccuracy = directionalAccuracies.reduce((a,b) => a+b);
      directionalAccuracy /= hyperparameters.numOutputs;
      List<String> directionalAccuracyStrings = new List.generate(directionalAccuracies.length, (i){
        return (directionalAccuracies[i]*100).toStringAsFixed(1);
      });

      _log.info(
          "directional accuracy ${(directionalAccuracy * 100)
              .toStringAsFixed(1)}% - $directionalAccuracyStrings");

      mm.metadata["statistics"] = {"absoluteRelativeError": lastError, "directionalAccuracy": directionalAccuracy};
    }
  }


  _loadNetworks(){
    // load the best trained model

    io.File file = new io.File("data/models/$_branch.json");
    if(!file.existsSync()){
      return false;
    }

    String input = file.readAsStringSync();
    List rawNetworks = JSON.decode(input);
    List networks = new List.generate(rawNetworks.length, (i){
      Map rawNetwork = rawNetworks[i];
      MonkeyMind mm = new MonkeyMind.deserialize(rawNetwork);
      mm.hyperparameters = new LaggedGeneticAlgorithmHyperparameterSet.deserialize(rawNetwork["hyperparameters"]);
      return mm;
    });

    _log.info("did load ${networks.length} networks from disk");

    _networks = networks;
    return true;
  }

  _saveNetworks(){

    List output = [];

    for(MonkeyMind mm in _networks){
      Map rawNetwork = mm.serialize();
      rawNetwork["hyperparameters"] = mm.hyperparameters.serialize();
      output.add(rawNetwork);
    }

    io.File file = new io.File("data/models/$_branch.json");
    file.writeAsStringSync(JSON.encode(output));

    _log.info("did save networks to disk");
  }

  _generateConversativeSeed(){
    _generateExperimentSeed([[10], [10,2]], [60], [["XXBTZUSD", "XETHZUSD", "XETHXXBT"]]);
  }

  _generateIncubationSeed(){
    List hiddenLayerConfigurations = [
      [4],
      [20],
      [20,4],
      [20,20]
    ];

    List respectedTimeFrames = [10,20,30,45,60,90];

    List availableAssetPairCombinations = [
      ["XXBTZUSD"],
      ["XXBTZUSD", "XETHZUSD", "XETHXXBT"]
    ];

    _generateExperimentSeed(hiddenLayerConfigurations, respectedTimeFrames, availableAssetPairCombinations);
  }

  _generateExperimentSeed(List hiddenLayerConfigurations, List respectedTimeFrames, List availableAssetPairCombinations){

    List networks = [];

    for(List hiddenLayerConfiguration in hiddenLayerConfigurations){
      for(int respectedTimeFrame in respectedTimeFrames){
        for(List availableAssetPairCombination in availableAssetPairCombinations){
          //consider only assetPairs that have USD base
          List<AssetPair> availableAssetPairs = _assetPairOHLCs.keys.where(
              (AssetPair ap) => availableAssetPairCombination.contains(ap.code)
          ).toList();

          LaggedGeneticAlgorithmHyperparameterSet hyperparameters =
          new LaggedGeneticAlgorithmHyperparameterSet(
              availableAssetPairs,
              respectedTimeFrame,
              1,
              hiddenLayerConfiguration);

          String name = availableAssetPairCombination.toString() + " " + respectedTimeFrame.toString() + " days, " + hiddenLayerConfiguration.toString();

          MonkeyMind mm;
          mm = _networkForHyperparameters(name + "Sigmoid", hyperparameters, false);
          networks.add(mm);
          mm = _networkForHyperparameters(name + "TanH", hyperparameters, true);
          networks.add(mm);
        }
      }
    }

    _log.info("did seed ${networks.length} new networks");

    _networks = networks;
  }

  _networkForHyperparameters(String name, LaggedGeneticAlgorithmHyperparameterSet hyperparams, bool tanH){
    _log.fine("generating seed monkey mind with network config ${hyperparams.networkConfiguration}");
    MonkeyMind mm = new MonkeyMind(name, hyperparams.networkConfiguration, tanH);
    mm.hyperparameters = hyperparams;
    return mm;
  }

}


class GeneticAlgorithm{



  /*
    const double TRAINING_TO_TEST_RATIO = 0.8;
    int border = (helper.timeline.length * TRAINING_TO_TEST_RATIO).toInt();
    List trainingMoments = helper.timeline.take(border).toList();
    trainingMoments.sort();
    List testMoments = helper.timeline.skip(border).toList();
    testMoments.sort();
    */



  runOnlineTraining(){
    MonkeyMind mm = new MonkeyMind.adaptable();
    AssetPairTimeSeriesHelper helper = new AssetPairTimeSeriesHelper(_assetPairOHLCs);

    for(int moment in helper.timeline){

    }

  }

  runTournament(){

    assert(_assetPairOHLCs != null);

    //consider only assetPairs that have USD base
    List<AssetPair> availableAssetPairs = _assetPairOHLCs.keys.where(
        (AssetPair ap) => ap.quote == Asset.USD && _assetPairOHLCs[ap].length > 0
    ).toList();
    Map<AssetPair, List<OHLC>> availableAssetPairOHLCs = new Map.fromIterable(
        availableAssetPairs,
        value: (ap) => _assetPairOHLCs[ap]
    );
    Map<AssetPair, double> lastAssetPairPrices = new Map.fromIterable(
        availableAssetPairs,
        value: (ap){
          List<OHLC> OHLCs = availableAssetPairOHLCs[ap];
          return OHLCs.last.close;
        }
    );

    MultiAssetPairTradingStrategyTournamentResult hodlResult = performHodlMeasurement(availableAssetPairOHLCs, lastAssetPairPrices);

    List strategies = _generatePseudoRecurrentStrategies(NUM_INITIAL_STRATEGIES);

    SinglePseudoRecurrentNeuralNetworkTradingStrategy bestStrategy = null;
    SingleAssetPairTradingStrategyTournamentResult bestResult = null;

    for(int i = 0; i < NUM_TOURNAMENTS; i++){
      _log.info("Round $i - ${strategies.length} strategies to test");
      MultiAssetPairTradingStrategyTournament tournament = new MultiAssetPairTradingStrategyTournament();
      tournament.strategies = strategies;
      tournament.portfolioTemplate = portfolioTemplate;
      tournament.assetPairOHLCs = availableAssetPairOHLCs;
      tournament.market = market;

      Map<TradingStrategy, MultiAssetPairTradingStrategyTournamentResult> tournamentResults = tournament.run();


    }

    logSingleAssetPairTradingStrategyResult(
        "overall best",
        bestStrategy,
        bestResult,
        lastAssetPairPrices);
    logStrategyOrders(bestResult, lastAssetPairPrices);

  }


  /*
    Portfolio optimalPortfolio = findOptimalTradingPath(OHLCs, market);
    logStrategy(
        "theoretical optimum",
        null,
        lastAssetPairPrices);
    logStrategyOrders(optimalPortfolio);
*/

  /*
      // Sort the trading strategies by success (best to worst)
      List<TradingStrategy> strategiesByPerformance = new List.from(strategies);
      strategiesByPerformance.sort((TradingStrategy a, TradingStrategy b){
        double aValue = tournamentResults[a].getOverallUSDPerformance(lastAssetPairPrices, PORTFOLIO_USD_INITIAL_VALUE);
        double bValue = tournamentResults[b].getOverallUSDPerformance(lastAssetPairPrices, PORTFOLIO_USD_INITIAL_VALUE);
        return bValue.compareTo(aValue);
      });


      var worst = strategiesByPerformance.last;
      var median = strategiesByPerformance[strategiesByPerformance.length ~/ 2];
      var best = strategiesByPerformance.first;
      logMultiAssetPairTradingStrategyResult(
          "worst",
          worst,
          tournamentResults[worst],
          lastAssetPairPrices,
          hodlResult: hodlResult);
      logMultiAssetPairTradingStrategyResult(
          "median",
          median,
          tournamentResults[median],
          lastAssetPairPrices,
          hodlResult: hodlResult);
      logMultiAssetPairTradingStrategyResult(
          "best",
          best,
          tournamentResults[best],
          lastAssetPairPrices,
          hodlResult: hodlResult);
      logStrategyOrders(tournamentResults[best], lastAssetPairPrices);

      /* log all performances
      for(MonkeyMind network in _networks){
        Portfolio portfolio = networkPortfolios[network];
        double value = portfolioUSDValue(portfolio, {btcusd: OHLCs.last});
        _log.finest("${network.name} - $value ${network.serialize()}");
      }
      */

      ///select the best for the next round
      int survivingStrategiesTarget = math.min(strategies.length, TARGET_POPULATION_SIZE);
      strategies = strategiesByPerformance.take(survivingStrategiesTarget).toList();
      _log.fine("${strategies.length} strategies survived");


      List<TradingStrategy> strategiesToAdd = [];

      int previousBorderIndex = 0;

      for(double selectionBorder in REPRODUCTION_RATES.keys){
        int mutatedStrategiesToAdd = REPRODUCTION_RATES[selectionBorder];

        int borderIndex = (strategies.length * selectionBorder).toInt();
        int take = borderIndex - previousBorderIndex;

        for(TomensPseudoRecurrantNeuralNetworkTradingStrategy parent in strategies.skip(previousBorderIndex).take(take)){
          for(int i = 0; i < mutatedStrategiesToAdd; i++){
            MonkeyMind parentMind = parent.mm;
            MonkeyMind childMind = parentMind.copy("_$i");
            childMind.mutate(MUTATION_RATE, MUTATION_STRENGTH, MonkeyMind.mutateConnectionRelative);
            strategiesToAdd.add(new TomensPseudoRecurrantNeuralNetworkTradingStrategy(childMind, parent.numFeedforwardValues));
          }
        }

        previousBorderIndex = borderIndex;
      }

      strategiesToAdd.addAll(_generateStrategies(NUM_NEW_RANDOM_STRATEGIES_PER_ROUND));

      strategies.addAll(strategiesToAdd);

      _log.fine("${strategiesToAdd.length} mutated networks added.");

      bestStrategy = strategiesByPerformance.first;
      bestResult = tournamentResults[bestStrategy];
      */

  static List<SinglePseudoRecurrentNeuralNetworkTradingStrategy> _generatePseudoRecurrentStrategies(int count){
    var networks = MonkeyMind.generateRandomModels(count, NUM_FEEDFORWARD_VALUES);
    return new List.generate(networks.length, (i) {
      return new SinglePseudoRecurrentNeuralNetworkTradingStrategy(networks[i], NUM_FEEDFORWARD_VALUES);
    });
  }

  MultiAssetPairTradingStrategyTournamentResult performHodlMeasurement(
      Map<AssetPair, List<OHLC>> assetPairOHLCs,
      Map<AssetPair, double> lastAssetPairPrices)
  {
    //debug / introspection
    TradingStrategy hodlStrategy = new HodlTradingStrategy();

    MultiAssetPairTradingStrategyTournament tournament = new MultiAssetPairTradingStrategyTournament();
    tournament.strategies = [hodlStrategy];
    tournament.portfolioTemplate = portfolioTemplate;
    tournament.assetPairOHLCs = assetPairOHLCs;
    tournament.market = market;

    Map<TradingStrategy, MultiAssetPairTradingStrategyTournamentResult> tournamentResults = tournament.run();

    logMultiAssetPairTradingStrategyResult(
        "hodl",
        hodlStrategy,
        tournamentResults[hodlStrategy],
        lastAssetPairPrices);

    return tournamentResults[hodlStrategy];
  }

  static logSingleAssetPairTradingStrategyResult(
      String description,
      TradingStrategy strategy,
      SingleAssetPairTradingStrategyTournamentResult result,
      Map<AssetPair, double> lastAssetPairPrices,
      {SingleAssetPairTradingStrategyTournamentResult hodlResult}){

    String message = "$description: ";

    double overallPerformance = result.getOverallUSDPerformance(lastAssetPairPrices, PORTFOLIO_USD_INITIAL_VALUE);

    ///ToDo: completed orders -> "${result.completedOrders.length} orders -"

    message += "${overallPerformance.toStringAsFixed(4)} ";

    if(hodlResult != null){
      double overallHodlPerformance = hodlResult.getOverallUSDPerformance(lastAssetPairPrices, PORTFOLIO_USD_INITIAL_VALUE);
      double relativeToHodl = overallPerformance / overallHodlPerformance;
      message += "- ${relativeToHodl.toStringAsFixed(4)} relativeToHodl ";
    }

    /*
  if(network != null){
    message += "name: ${network.name} ";
  }
  */

    _log.info(message);
  }

  static logMultiAssetPairTradingStrategyResult(
      String description,
      TradingStrategy strategy,
      MultiAssetPairTradingStrategyTournamentResult result,
      Map<AssetPair, double> lastAssetPairPrices,
      {MultiAssetPairTradingStrategyTournamentResult hodlResult}){

    String message = "$description: ";

    double overallPerformance = result.getOverallUSDPerformance(lastAssetPairPrices, PORTFOLIO_USD_INITIAL_VALUE);

    ///ToDo: completed orders -> "${result.completedOrders.length} orders -"

    message += "${overallPerformance.toStringAsFixed(4)} ";

    if(hodlResult != null){
      double overallHodlPerformance = hodlResult.getOverallUSDPerformance(lastAssetPairPrices, PORTFOLIO_USD_INITIAL_VALUE);
      double relativeToHodl = overallPerformance / overallHodlPerformance;
      message += "- ${relativeToHodl.toStringAsFixed(4)} relativeToHodl ";
    }

    /*
  if(network != null){
    message += "name: ${network.name} ";
  }
  */

    _log.info(message);
  }

  static logStrategyOrders(SingleAssetPairTradingStrategyTournamentResult result, Map<AssetPair, double> lastAssetPairPrices){
    String message = "A deeper look into the winning strategy: \n";
    for(AssetPair assetPair in result.finalPortfolios.keys){
      List<CompletedOrder> completedOrders = result.completedOrders[assetPair];
      double performance = result.getAssetPairPerformance(assetPair,lastAssetPairPrices[assetPair], PORTFOLIO_USD_INITIAL_VALUE);

      message += "${assetPair.code} - performance ${performance.toStringAsPrecision(4)} - ${completedOrders.length} orders\n";

      for(CompletedOrder order in completedOrders){
        _log.info("${new DateTime.fromMillisecondsSinceEpoch(order.time*1000)} ${order.type.toString()} at ${order.price}");
      }
    }

    _log.info(message);

  }

}
