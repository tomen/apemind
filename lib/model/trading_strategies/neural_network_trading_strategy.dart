library apemind.model.trading_strategies.neural_network;

import "dart:math" as math;
import "package:neural_network/neural_network.dart";
import "package:apemind/model.dart";
import "package:apemind/trading.dart";

part "monkey_mind.dart";

math.Random random = new math.Random();

class MultiOnlineNeuralNetworkTradingStrategy implements TradingStrategy {
  MonkeyMind _mm;
  MonkeyMind get mm => _mm;

  Map<AssetPair, TradeSignal> get currentTradeSignals => {};

  MultiOnlineNeuralNetworkTradingStrategy(this._mm){
    reset();
  }

  reset(){

  }

  receiveInformation(Map<AssetPair, OHLC> assetPairOHLCs){

  }
}

class SinglePseudoRecurrentNeuralNetworkTradingStrategy implements TradingStrategy {
  MonkeyMind _mm;
  MonkeyMind get mm => _mm;
  int _numFeedforwardValues;
  int get numFeedforwardValues => _numFeedforwardValues;
  List _feedforwardValues;
  TradeSignal _currentTradeSignal;
  TradeSignal get currentTradeSignal => _currentTradeSignal;

  SinglePseudoRecurrentNeuralNetworkTradingStrategy(this._mm, this._numFeedforwardValues){
    reset();
  }


  reset(){
    _feedforwardValues = new List.filled(_numFeedforwardValues, 0.0);
    _currentTradeSignal = TradeSignal.Nothing;
  }

  receiveInformation(OHLC ohlc){
    //prepare the input network
    List inputNetwork = ohlc.toJson();
    inputNetwork.removeAt(0);
    inputNetwork[inputNetwork.length-1] = inputNetwork.last.toDouble();
    inputNetwork.addAll(_feedforwardValues);

    _mm.inputNetwork = inputNetwork;
    _mm.calculateOutput();
    List outputNetwork = _mm.outputNetwork;

    double signalValue = outputNetwork[0];
    outputNetwork.removeAt(0);
    _feedforwardValues = outputNetwork;

    if(signalValue >= 0.75){
      //signal = Signal.Buy;
      _currentTradeSignal = TradeSignal.Buy;
    }
    else if(signalValue <= 0.25){
      //signal = Signal.Sell;
      _currentTradeSignal = TradeSignal.Sell;
    }
    else {
      _currentTradeSignal = TradeSignal.Nothing;
    }
  }
}
