part of apemind.model.trading_strategies.neural_network;

const int NUM_OHLC_INPUTS = 7;

class MonkeyMindMetadata {

}

class MonkeyMind extends Network {

  String name;
  Map metadata = {};
  var hyperparameters;

  MonkeyMind.adaptable();

  MonkeyMind(this.name, List<int>neuronsInLayers, bool tanH) {
    this.createNetwork(neuronsInLayers, tanH);
  }

  void createNetwork(List<int>neuronsInLayers, bool tanH) {

    Layer inputLayer = new Layer("InputLayer");
    inputLayer.createNeurons(neuronsInLayers[0]);
    this.addLayer(inputLayer);

    for (int i = 1; i < neuronsInLayers.length; i++) {
      Layer temp = new Layer("HiddenLayer" + i.toString());
      temp.createNeurons(neuronsInLayers[i], inputFunction: new WeightCombination(), activationFunction: (tanH ? new Tanh() : new Sigmoid()));
      this.addLayer(temp);
      for (Neuron neuron in this.layers[i].neurons) {
        Neuron umbral = new Neuron("Umbral " + i.toString());
        umbral.input = 1.0;
        umbral.output = 1.0;
        neuron.addInputConnectionFromNeuron(umbral);
        //        neuron.addInputConnection(new Connection(umbral, neuron, INITIAL_WEIGHT_RANGE*2*random.nextDouble()-INITIAL_WEIGHT_RANGE));
      }
    }

    this.layers.last.neurons.first.activationFunction = new Tanh();

    this.connectLayers();
  }

  Map serialize(){

    List sizes = [];
    List serializedNetwork = [];

    bool firstLayer = true;

    for(Layer layer in layers){
      sizes.add(layer.neurons.length);
      if(firstLayer){
        firstLayer = false;
        continue;
      }

      var serializedLayer = [];

      for(Neuron neuron in layer.neurons){
        List serializedNeuron = [];
        for(Connection connection in neuron.inputConnections){
          serializedNeuron.add(connection.weight.value);
        }
        serializedLayer.add(serializedNeuron);
      }

      serializedNetwork.add(serializedLayer);
    }

    return {"name": name, "sizes": sizes, "serializedNetwork": serializedNetwork, "metadata": metadata};
  }

  factory MonkeyMind.deserialize(Map raw){
    MonkeyMind mm = new MonkeyMind(raw["name"], raw["sizes"]);
    mm.metadata = raw["metadata"];

    for(int i = 1; i < mm.layers.length; i++){
      int j = 0;
      Layer layer = mm.layers[i];
      for(Neuron neuron in layer.neurons){
        int k = 0;
        for(Connection connection in neuron.inputConnections){
          connection.weight = new Weight(raw["serializedNetwork"][i-1][j][k]);
          k++;
        }
        j++;
      }
    }

    return mm;
  }

  MonkeyMind copy(String suffix){
    MonkeyMind mm = new MonkeyMind.deserialize(this.serialize());
    mm.name = mm.name + suffix;
    return mm;
  }

  static List<Network> generateRandomModels(int count, int numFeedforwardValues){
    return new List.generate(count, (int i){
      MonkeyMind net = new MonkeyMind(
          "$i",
          [NUM_OHLC_INPUTS + numFeedforwardValues, numFeedforwardValues + 1]);
      net.randomizeWeights();
      return net;
    });
  }

  randomizeWeights(){
    for(int i = 1; i < layers.length; i++){
      Layer layer = layers[i];
      for(Neuron neuron in layer.neurons){
        for(Connection connection in neuron.inputConnections){
          connection.weight.value = random.nextDouble() * 0.001- 0.0005;
        }
      }
    }
  }

  void mutate(double mutationRate, double mutationStrength, Function connectionMutator){
    for(Layer layer in this.layers)
    {
      for(Neuron neuron in layer.neurons)
      {
        for(Connection connection in neuron.inputConnections){
          connectionMutator(mutationRate, mutationStrength, connection);
        }
      }
    }
  }

  /// mutationRate is the chance that a given neuron might mutate. 0.1 would be a 10% chance that the neuron will change
  /// mutationStrength tells how many % a value might change. 1.0 would change it by up to 100%
  static void mutateConnectionRelative(double mutationRate, double mutationStrength, Connection connection)
  {
    if (random.nextDouble() < mutationRate) {
      connection.weight.value *= 1 + mutationStrength * (random.nextDouble() * 2 - 1);
      //if a connection weight is smaller than 0.001, it may change sign
      if(connection.weight.value.abs() < 0.001) {
        if(random.nextDouble() > 0.5) connection.weight.value *= -1;
      }
    }
  }

  static void mutateConnectionAbsolute(double mutationRate, double mutationStrength, Connection connection)
  {
    if (random.nextDouble() < mutationRate) {
      double delta = (random.nextDouble() * 2 - 1) * mutationStrength;
      connection.weight.value = (connection.weight.value + delta).clamp(-1.0, 1.0);
    }
  }
}
