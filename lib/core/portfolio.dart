part of apemind.core;

class Portfolio {
  logging.Logger _log = new logging.Logger("apemind.core.Portfolio");

  Map<Asset, double> _assetValues = {};
  Iterable<Asset> get assets => _assetValues.keys;

  Portfolio(this._assetValues);

  Portfolio copy(){
    Portfolio copy = new Portfolio(new Map.from(_assetValues));
    return copy;
  }

  double assetValue(Asset asset){
    if(_assetValues.containsKey(asset)) return _assetValues[asset];
    return 0.0;
  }

  increaseAssetValue(Asset asset, double value){
    if(!_assetValues.containsKey(asset)) _assetValues[asset] = 0.0;
    _assetValues[asset] += value;
  }

  decreaseAssetValue(Asset asset, double value){
    if(!_assetValues.containsKey(asset)) _assetValues[asset] = 0.0;
    _assetValues[asset] -= value;
    if(_assetValues[asset] < 0.0){
      _log.warning("Decreased asset value of $asset below 0.0");
    }
  }
}


