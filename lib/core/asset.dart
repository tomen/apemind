part of apemind.core;


class Asset {
  String code;

  String toString(){
    return this.code;
  }

  static Asset USD = new Asset("ZUSD");
  static Asset BTC = new Asset("XXBT");

  bool isEqual(Asset other) => this.code == other.code;

  static Map<String, Asset> _assets = {};

  Asset._internal(this.code);
  factory Asset(String code){
    if(_assets[code] != null) return _assets[code];

    Asset asset = new Asset._internal(code);
    _assets[code] = asset;
    return asset;
  }

}
