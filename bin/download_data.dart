import "dart:io";
import "dart:convert";
import "dart:async";
import "package:logging/logging.dart" as logging;

import "package:apemind/trading.dart";


logging.Logger _log = new logging.Logger("");

class TimeInterval {
  static const int DAY = 1440;
}

main(List<String> args) async {
  print('Flag day!');

  _log.onRecord.listen((logging.LogRecord record){
    print("${record.level.toString()} - ${record.message}");
  });
  _log.level = logging.Level.INFO;

  //Time
  //Assets
  //AssetPairs
  //Ticker?pair=XXBTZUSD
  //OHLC?pair=XXBTZUSD

  // 1st May 2016 - 1462053600

  List<AssetPair> assetPairs = await fetchAssetPairs();
  Map<AssetPair, List<OHLC>> assetPairOHLCs = {};

  for(AssetPair ap in assetPairs){
    assetPairOHLCs[ap] = await fetchOHLCs(ap.code);
  }

  File file = new File("data/prices/asset_pairs.json");
  String json = JSON.encode(assetPairs);
  file.writeAsStringSync(json);

  for(AssetPair ap in assetPairOHLCs.keys){
    List<OHLC> OHLCs = assetPairOHLCs[ap];
    File file = new File("data/prices/${ap.code}.json");
    String json = JSON.encode(OHLCs);
    file.writeAsStringSync(json);
  }

  _log.info("gathered " + assetPairOHLCs.length.toString() + " asset pair OHLCs.");
}

class FetchResult {
  Map _raw;

  bool get hasErrors => _raw["error"].length > 0;
  Map get result => _raw["result"];

  FetchResult(this._raw);
}

class OHLCFetchResult extends FetchResult {
  String _pair;

  List get moments => result[_pair];
  int get last => result["last"];

  OHLCFetchResult(Map raw, this._pair) : super (raw);
}

Future<List<AssetPair>> fetchAssetPairs() async {
  List<AssetPair> assetPairs = [];
  Map result = await fetch("AssetPairs");
  for(String key in result["result"].keys){
    Map rawAssetPair = result["result"][key];
    AssetPair assetPair = new AssetPair.fromKrakenRawData(key, rawAssetPair);
    assetPairs.add(assetPair);
  }

  _log.fine("got ${assetPairs.length} asset pairs");
  return assetPairs;
}

Future<List<OHLC>> fetchOHLCs(String pair, {int interval: TimeInterval.DAY}) async {
  int last = 0;
  List ohlcs = [];

  await Future.doWhile(() async{
    _log.fine(last);
    OHLCFetchResult fetch = await fetchOHLC(pair: pair, since: last, interval: interval);
    List<OHLC> newOHLCs = null;
    if(fetch.hasErrors){
      return false;
    }
    else
    {
      newOHLCs = new List.generate(fetch.moments.length, (int i) => new OHLC.fromJSON(fetch.moments[i]));
    }
    ohlcs.addAll(newOHLCs); // does this result in a tidy chain?

    last = newOHLCs.last.time;

    if(last >= fetch.last)
    {
      return false;
    }

    return true;
  });

  return ohlcs;
}

Future<OHLCFetchResult> fetchOHLC({String pair, int since, int interval}) async {
  Map result = await fetch("OHLC", {"pair":pair, "since": "$since", "interval": "$interval"});

  if(!result.containsKey("result")){
    _log.info("could not fetch OHLC for pair $pair");
  }

  return new OHLCFetchResult(result, pair);
}

int lastRequest = 0;

Future<Map> fetch(String method, [Map queryParameters]) async {

  // wait a second before making the next request
  Future.doWhile((){
    int currentTime = (new DateTime.now()).millisecondsSinceEpoch ~/ 1000;
    if(currentTime <= lastRequest) return true;

    lastRequest = currentTime;
    return false;
  });

  //TODO wrap the parameters into null checking
  Uri uri = new Uri(scheme: "https", host:"api.kraken.com", path:"0/public/$method", queryParameters: queryParameters);
  _log.info(uri);
  HttpClient client = new HttpClient();
  HttpClientRequest request = await client.getUrl(uri);
  //request.write('{"pair":"XXBTZUSD"}');
  HttpClientResponse response = await request.close();

  Map result = await response.transform(UTF8.decoder).transform(JSON.decoder).single;
  //print(data);

  if(!result["error"].isEmpty){
    _log.warning("Server sent error message: ${result["error"]}");
  }

  return result;
}