

import "dart:io";
import "dart:convert";
import "dart:math" as math;
import "package:logging/logging.dart" as logging;


import "package:apemind/core.dart";
import "package:apemind/trading.dart";
import "package:apemind/model/trading_strategies/neural_network_trading_strategy.dart";



logging.Logger _log = new logging.Logger("");


main(List<String> args) async {
  print('Flag day!');

  // configure logging
  _log.onRecord.listen((logging.LogRecord record){
    print("${record.time.toString()} ${record.level.toString()} - ${record.loggerName} - ${record.message}");
  });
  _log.level = logging.Level.INFO;

  try {

    // load historic asset price information
    File file = new File("data/asset_pairs.json");
    var json = file.readAsStringSync();
    var rawList = JSON.decode(json);
    List<AssetPair> assetPairs = new List.generate(rawList.length, (int i) => new AssetPair.fromSerializedJSON(rawList[i]));
    _log.fine("${assetPairs.length} asset pairs loaded");

    Map<AssetPair, List<OHLC>> assetPairOHLCs = new Map.fromIterable(assetPairs, value: (ap){
      File file = new File("data/${ap.code}.json");
      var json = file.readAsStringSync();
      var rawList = JSON.decode(json);
      return new List.generate(rawList.length, (int i) => new OHLC.fromSerializedJSON(rawList[i]));
    });

    // load the best trained model
    file = new File("data/models/best_network.json");
    String rawNetworkString = file.readAsStringSync();
    Map rawNetwork = JSON.decode(rawNetworkString);
    MonkeyMind mm = new MonkeyMind.deserialize(rawNetwork);

    // filter out the BTCUSD asset pair
    List<AssetPair> availableAssetPairs = assetPairOHLCs.keys.where(
        (AssetPair ap) =>
    ap.quote == Asset.USD &&
        ap.base == Asset.BTC &&
        assetPairOHLCs[ap].length > 0
    ).toList();
    assetPairOHLCs = new Map.fromIterable(
        availableAssetPairs,
        value: (ap) => assetPairOHLCs[ap]
    );

    // now do the math and prepare the data for the model (last 60 days)
    Map logReturns = calculateLogReturns(assetPairOHLCs);
    List inputLogReturns = logReturns["inputs"];
    inputLogReturns = inputLogReturns.skip(inputLogReturns.length - 60).toList();
    List input = [];
    for(List frame in inputLogReturns){
      input.addAll(frame);
    }

    // run the model
    mm.inputNetwork = input;
    mm.calculateOutput();
    double todaysLogReturn = mm.outputNetwork[0];

    // calculate the predicted price
    double yesterdaysPrice = assetPairOHLCs.values.last.last.close;
    double todaysPricePrediction = math.exp(todaysLogReturn) * yesterdaysPrice;

    _log.info("Todays log return: $todaysLogReturn - predicted closing price: $todaysPricePrediction");
  }
  catch(ex, stack){
    _log.warning(ex);
    _log.warning(stack);
  }

  _log.info("end of code");
}


