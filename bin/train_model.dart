

import "dart:io";
import "dart:convert";
// import "dart:async";
import "package:logging/logging.dart" as logging;


import "package:apemind/trading.dart";
import "package:apemind/model/trading_strategies/genetic_algorithm.dart";
import "package:apemind/model/trading_strategies/neural_network_trading_strategy.dart";

logging.Logger _log = new logging.Logger("");


main(List<String> args) async {
  print('Flag day!');

  _log.onRecord.listen((logging.LogRecord record){
    print("${record.time.toString()} ${record.level.toString()} - ${record.loggerName} - ${record.message}");
  });
  _log.level = logging.Level.INFO;

  try {

    File file = new File("data/prices/asset_pairs.json");
    var json = file.readAsStringSync();
    var rawList = JSON.decode(json);
    List<AssetPair> assetPairs = new List.generate(rawList.length, (int i) => new AssetPair.fromSerializedJSON(rawList[i]));
    _log.fine("${assetPairs.length} asset pairs loaded");

    Map<AssetPair, List<OHLC>> assetPairOHLCs = new Map.fromIterable(assetPairs, value: (ap){
      File file = new File("data/prices/${ap.code}.json");
      var json = file.readAsStringSync();
      var rawList = JSON.decode(json);
      return new List.generate(rawList.length, (int i) => new OHLC.fromSerializedJSON(rawList[i]));
    });


    LaggedGeneticAlgorithm trainer = new LaggedGeneticAlgorithm(assetPairOHLCs);
    trainer.runOfflineTraining();
    //trainer.runTournament();
  }
  catch(ex, stack){
    _log.warning(ex);
    _log.warning(stack);
  }

  _log.info("end of code");
}


